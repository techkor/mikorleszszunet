package hu.kodolas.mikorleszszunet;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Handler handler;
    private TextView countdown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        handler = new Handler();
        countdown = (TextView) findViewById(R.id.countdown);

        updateCountdown();
    }

    private void updateCountdown() {
        countdown.setText((1497520800-System.currentTimeMillis()/1000)+" mp");

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                updateCountdown();
            }
        }, 1000);
    }


}
